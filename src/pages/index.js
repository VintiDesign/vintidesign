import React from "react";

import Header from "../components/header";
import Hero from "../components/hero";

const IndexPage = () => (
  <div className="wrappr">
    <Header />
    <Hero />
  </div>
);

export default IndexPage;
